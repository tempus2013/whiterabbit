# -*- coding: utf-8 -*-

from django.template.loader import render_to_string
from django.core.files.temp import NamedTemporaryFile
from django.contrib import messages
from django.shortcuts import redirect
from django.http import HttpResponse
from django.template import RequestContext

import codecs
import os

from syjon.settings import STATIC_ROOT, STATIC_URL
from syjon.config import MACHINE_ARCHITECTURE
from apps.syjon.lib.functions import utf2ascii

def get_pdf_response(request, template_context, template_file, output_file_name, error_redirect, wk_args={}):
    """
    @param request: request
    @param template_context: słownik z danymi wykorzystywanymi w szablonie
    @param template_file: pełna ścieżka do pliku szablonu
    @param output_file_name: nazwa wygenerowanego pliku PDF
    @param error_redirect: adres przekierowania w razie wystąpienia błędu     
    """
    # Wygenerowanie pliku HTML
    page = render_to_string(template_file, template_context, context_instance=RequestContext(request))
    
    # Stworzenie nazw plików tymczasowych
    created_name = NamedTemporaryFile(delete=True)
    filename_HTML = created_name.name+'.html'
    filename_PDF = created_name.name+'.pdf'
    
    # Zapisanie pliku HTML
    output_file = codecs.open(filename_HTML, 'w','utf-8')
    output_file.write(page)
    output_file.close()
    
    # Ustalenie architektury
    WKHTML_TO_PDF = 'wkhtmltopdf-amd64'
    if MACHINE_ARCHITECTURE == 'i386':
        WKHTML_TO_PDF = 'wkhtmltopdf-i386'
 
    WKHTML_ARGS=''
    for arg in wk_args:
        WKHTML_ARGS +=' '+arg+' '+wk_args[arg]

    WKHTML_ARGS += '-L 7mm -R 7mm -T 10mm -B 10mm'

    # Stworzenie pliku PDF
    command = STATIC_ROOT+'whiterabbit/%s %s %s %s' % (WKHTML_TO_PDF, WKHTML_ARGS, filename_HTML, filename_PDF)
    os.system(command)
    
    # Wczytanie pliku PDF
    try:
        pdf_data = open(created_name.name + '.pdf', 'rb').read()
    except:
        messages.error(request, 'Wystąpił błąd podczas generowania pliku PDF. Spróbuj ponowanie za chwilę.')
        return redirect(error_redirect)

    # Usunięcie plików tymczasowych
    created_name.close()
    os.system('rm '+filename_HTML)
    os.system('rm '+filename_PDF)
    
    response = HttpResponse(pdf_data, mimetype='application/pdf')
    response['Content-Disposition'] = 'filename="%s.pdf"' % output_file_name
    return response

def get_pdf_file(template_context, template_file, filename_PDF,  wk_args={}, path='/tmp/sylabusy'):
    """
    @param template_context: słownik z danymi wykorzystywanymi w szablonie
    @param template_file: pełna ścieżka do pliku szablonu
    @param output_file_name: nazwa wygenerowanego pliku PDF  
    """
    # Wygenerowanie pliku HTML
    template_context['STATIC_URL'] = STATIC_URL
    page = render_to_string(template_file, template_context)

    # Stworzenie katalogu
    d = utf2ascii(path)
    if not os.path.exists(d):
        os.makedirs(d)

    # Stworzenie nazw plików tymczasowych
    #created_name = NamedTemporaryFile(delete=True)
    #filename_HTML = created_name.name+'.html'
    #filename_PDF = d + "/" + utf2ascii(filename_PDF)
    
    # Zapisanie pliku HTML
    #output_file = codecs.open(filename_HTML, 'w', 'utf-8')
    #output_file.write(page)
    #output_file.close()
    
    # Ustalenie architektury
    WKHTML_TO_PDF = 'wkhtmltopdf-amd64'
    if MACHINE_ARCHITECTURE == 'i386':
        WKHTML_TO_PDF = 'wkhtmltopdf-i386'
    
    WKHTML_ARGS=''
    for arg in wk_args:
        WKHTML_ARGS+=' '+arg+' '+wk_args[arg] 
    
    # Stworzenie pliku PDF
    #command = STATIC_ROOT+'/whiterabbit/wkhtmltopdf-i386 %s %s' % (filename_HTML, filename_PDF)
    #command = STATIC_ROOT+'whiterabbit/%s %s %s "%s"' % (WKHTML_TO_PDF, WKHTML_ARGS, filename_HTML, filename_PDF)
    #os.system(command)

    # Usunięcie plików tymczasowych
    #created_name.close()
    #os.system('rm '+filename_HTML)